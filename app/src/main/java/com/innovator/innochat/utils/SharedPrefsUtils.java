package com.innovator.innochat.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.innovator.innochat.R;

public class SharedPrefsUtils {
    private static void writeString(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }
    private static String readString(Context context, String key, String defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getString(key, defaultValue);
    }
}
