package com.innovator.innochat.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class JsonUtils {

    private static String convertDateFormat(String sourceDateString) {
        DateFormat originalFormat = new SimpleDateFormat(DATE_ISO_FORMAT);
        originalFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat targetFormat = SimpleDateFormat.getDateTimeInstance();
        Date date;
        String formattedDate = "";
        try {
            date = originalFormat.parse(sourceDateString);
            formattedDate = targetFormat.format(date);
        } catch (ParseException e) {
            // failed to parse - unexpected format
        }

        return formattedDate;
    }
    private static final String DATE_ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
}
