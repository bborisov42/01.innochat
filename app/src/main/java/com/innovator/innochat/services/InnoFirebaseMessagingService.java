package com.innovator.innochat.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.innovator.innochat.R;

import java.util.Map;

public class InnoFirebaseMessagingService extends FirebaseMessagingService {
    private static final String NOTIFICATION_TYPE_SHAKE = "shake";

    private static final String CHANNEL_ID = "innochatchannelid";
    private static final int NOTIFICATION_ID = 4242;

    @Override
    public void onCreate() {
        super.onCreate();

        registerChannel();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("MSG_RECEIVED", remoteMessage.toString());

        // TODO: If type of message is "shake", vibrate and show notification in bar
    }


    private void registerChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "innochat";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            // Register the channel with the system
            NotificationManager notificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
